# test-php

You need to create a responsive mobile webview to show a list of users from a partner.
The lists of users are regularly being added to the API, so your system will have to request the API often to pull the updated data.

- Request the data from the route : https://reqres.in/api/users?page=1&per_page=6 (paginated request)
- Parse the response and store the necessary content so that you won't have to call the external partner API everytime the page is loaded as the authorized number of requests to the API are limited
- *User* entities data can be updated or created in the future in your system. Structure your code to plan any necessary needs to easily create, update, delete a *User* entity in the future from other parts of your project. (Services, Factory, Manager, Exceptions ...) 

## Rules to follow
- You must commit regularly so we can follow your progress with an initial empty commit when you start the test

## Deliverables

- The link to the git repository
- Several commits, with an explicit message each time
- Docker project or setup instructions.
- A separate file or email documenting your process and principles you've followed

![alt-text]( https://bitbucket.org/beewake/test-php/raw/0c9bb9cf57e12a229c7fc9ecc55ed82870f507c0/user-list.png )



